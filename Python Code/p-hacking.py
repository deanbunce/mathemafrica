# Script to show p-hacking
import numpy as np
import pandas as pd
import statsmodels.api as sm
import os
import matplotlib.pyplot as plt


root_path = '../'
fname_fig = os.path.join(root_path,'Images','plots2.png')

# Generate some data that we can use for our p-hacking
pigeon_vec = np.arange(5).reshape(5,)

# Decide what measurements we want for each of our students
names = ['Cholesterol','Red blood cell count','IQ','GPA','Jammies Taken',
         'Blood sugar','Lung Capacity','Eyesight','Starting Salary',
         'Heart rate','Body fat %','Number of years at UCT']

# Create these measurements from a standard normal distribution
outputs = pd.DataFrame()
for name in names:
    outputs[name] = np.random.normal(loc=0,scale=1,size =5)

# Create a plot so we can see how random it is
cols = 3
rows = 4
fig, axs = plt.subplots(nrows=rows, ncols=cols, figsize=(10, 10))

x_fit = sm.add_constant(pigeon_vec)
with open(os.path.join(root_path, 'Images', "p-hacking2"), 'w') as outfile:

    for i,col in enumerate(outputs.columns):
        y = outputs[col]
        # Fit model to some data
        model = sm.OLS(y,x_fit)
        results = model.fit()
        summary = results.summary()

        # Plot the distributions
        x = pigeon_vec
        y = y

        axs[i // cols, i % cols].plot(x,y,'o')
        axs[i // cols, i % cols].set(title='{}'.format(col), ylabel='Amount')
        axs[i // cols, i % cols].margins(0.05)

        # Write out a summary to a text file
        outfile.write("RESULTS FOR THE COLUMN: {}\n".format(col))
        outfile.write("{}".format(summary))
        outfile.write("\n=====================================================\n")

plt.savefig(fname_fig, bbox_inches='tight')
