# K-means script
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import os

root_path = '../'
full_data = pd.read_csv('../Data/FIFA_19.csv')
data = full_data.iloc[0:2000,:]
# print(full_data.loc[2001:2100,:])
max_clusters = 15

all = ['Name','Crossing','Finishing','HeadingAccuracy','ShortPassing','Volleys','Dribbling','Curve','FKAccuracy',
              'LongPassing','BallControl','Acceleration','SprintSpeed','Agility','Reactions','Balance','ShotPower',
              'Jumping','Stamina','Strength','LongShots','Aggression','Interceptions','Positioning','Vision',
              'Penalties','Composure','Marking','StandingTackle','SlidingTackle','GKDiving','GKHandling',
              'GKKicking','GKPositioning','GKReflexes']
attr =  ['Crossing','Finishing','HeadingAccuracy','ShortPassing','Volleys','Dribbling','Curve','FKAccuracy',
              'LongPassing','BallControl','Acceleration','SprintSpeed','Agility','Reactions','Balance','ShotPower',
              'Jumping','Stamina','Strength','LongShots','Aggression','Interceptions','Positioning','Vision',
              'Penalties','Composure','Marking','StandingTackle','SlidingTackle','GKDiving','GKHandling',
              'GKKicking','GKPositioning','GKReflexes']

k_data = data[attr]
# losses = []
# for n in range(1,max_clusters):
#
#     Kmeans = KMeans(n_clusters=n, max_iter=600, algorithm = 'auto')
#     means = Kmeans.fit(k_data)
#     loss = means.inertia_
#     losses.append(loss)

df = pd.DataFrame()

# Plot the losses
# x = np.arange(1,max_clusters)
# plt.plot(losses)
# plt.title('K Means losses for a different number of clusters')
# plt.xlabel('Number of clusters')
# plt.ylabel('Value of Kmeans')


# The value we have chosen based on our clever elbow method is

n_clusters_use = 4
Kmeans = KMeans(n_clusters=n_clusters_use, max_iter=600, algorithm='auto')
means = Kmeans.fit(k_data)
df['Name'] = data.Name
df['Group'] = means.labels_

print(df.head(15))

for label in range(n_clusters_use):

    print("New label {} \n".format(label))
    print(df.loc[df['Group']==label,'Name'].head(15))
    print('End of label \n\n')

names = ['D. Calvert-Lewin','D. Welbeck','P. Tau','D. Keet','E. Mathoho','L. Mothiba','T. Lorch','L. Mothiba']
for pred_name in names:
    pred_player = full_data.loc[full_data['Name']==pred_name,attr]

    pred = means.predict(pred_player)
    print('The predicted group for {} is {}: '.format(pred_name,pred))

fname_fig =  os.path.join(root_path,'K_means_elbow.png')
plt.savefig(fname_fig, bbox_inches='tight')

