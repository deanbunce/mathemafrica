# p-value script
library(matrixStats)
library(pwr)
# First we define some values
n = 30
mu_values = c(0,0.05,0.1,0.3)
results_mat = matrix(nrow = 5, ncol = 4)
for (i in 1:range(4))
{
  mu_val = mu_values[i]
  sd = 1
  experiments = 10^4
  data = rnorm(n=n, mean = mean,sd = sd)
  #hist(data,main = 'Distribution of our data')
  
  # Now we find the mean:
  data_matrix = matrix(data = rnorm(n=experiments*n, mean = mu_val,sd = sd), ncol=experiments,nrow =n)
  means = colMeans(data_matrix)
  
  sds = colSds(data_matrix, na.rm=TRUE)
  sds
  ts = means/(sds/sqrt(n))
  hist(ts)
  
  
  ##########
  ps = 1- pt(ts,df=n-1)
  typical = 1-pt(mu_val*sqrt(n),df=n-1)
  typical
  
  median(ps)
  mean(ps)
  summary(ps)
  boxplot(ps)
  x_lim = c(0,1)
  hist(ps, main= bquote('Simulated distribution of p-values mean = ' ~ .(mu_val)), xlim = x_lim,breaks = 50)
  abline(v=0.05,col = 'red')
  abline(v=typical,col = 'blue')
  legend('topright',legend=c("5% cut-off", "p-value true mean"),
         col=c("red", "blue"),lty=c(1,1))
  
  results_mat[1,i] = length(ps[which(ps<0.001)])/experiments
  results_mat[2,i] = length(ps[which(ps<0.005)])/experiments
  results_mat[3,i] = length(ps[which(ps<0.01)])/experiments
  results_mat[4,i] = length(ps[which(ps<0.05)])/experiments
  results_mat[5,i] = length(ps[which(ps<typical)])/experiments
}

print(results_mat)